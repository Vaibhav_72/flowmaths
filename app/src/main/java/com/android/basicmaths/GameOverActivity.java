package com.android.basicmaths;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class GameOverActivity extends AppCompatActivity {

    TextView score_result_tv;
    SharedPreferences sharedPreferences;
    ImageView high_score_iv;
    TextView best_score_result_tv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        high_score_iv = findViewById(R.id.high_score_iv);
        best_score_result_tv = findViewById(R.id.best_score_result_tv);
        getSupportActionBar().hide();
        int points = getIntent().getExtras().getInt("points");
        score_result_tv = findViewById(R.id.score_result_tv);

        sharedPreferences = getSharedPreferences("pref", 0);
        int pointsSP = sharedPreferences.getInt("pointsSP", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(points > pointsSP){
            pointsSP = points;
            editor.putInt("pointsSP", pointsSP);
            editor.commit();
            high_score_iv.setVisibility(View.VISIBLE);
        }
        score_result_tv.setText(""+ points);
        best_score_result_tv.setText(""+ pointsSP);
    }

    public void restart(View view) {
        Intent intent = new Intent(GameOverActivity.this, StartGameActivity.class);
        startActivity(intent);
        finish();
    }

    public void exit(View view) {
        finish();
    }
}
